## Instalacion
npm install

## Ejecucion
npm start

## ejemplo
req.query.id indica que recibe un parametro GET con nombre id
http://localhost:8000/request6?id=1

req.body.id  indica que recibe un parametro POST con nombre id
Una buena forma de probarlo es usando la aplicacion Postman
{
    "id": 1
}
const express = require('express')
const app = express()
const path = require('path')
const port = 8000

// middleware - parsea application/x-www-form-urlencoded
app.use(express.urlencoded({extended: true}))
// middleware - parsea application/json
app.use(express.json())

// data de prueba
const data = [
  {id: 1, valor: 10, desc: 'uno'},
  {id: 2, valor: 20, desc: 'dos'},
  {id: 3, valor: 30, desc: 'tres'},
  {id: 4, valor: 40, desc: 'cuatro'},
  {id: 5, valor: 50, desc: 'cinco'},
]

// raiz de API
app.get('/', (req, res) => {
  res.send('Probando Nodejs')
})

// retorna data de prueba
app.get('/request1', (req, res) => {
  res.send(data)
})

// retorna primer registro del array
app.get('/request2', (req, res) => {
  res.send(data[0])
})

// retorna la suma del valor de la key valor de todos los registros
app.get('/request3', (req, res) => {
  let suma = 0
  data.forEach(row => {
    suma += row.valor
  })

  res.send({total: suma})
})

// retorna JSON a partir de un string
app.get('/request4', (req, res) => {
  try {
    const datastring = '[{"id": 1, "valor": 10, "desc": "uno"}, {"id": 2, "valor": 20, "desc": "dos"}]'
    const datajson = JSON.parse(datastring)
    res.send(datajson)
    //res.send(datastring)
  } catch (e) {
    throw new Error("Oooops!")
  }
})

// retorna el parametro obtenido de la url - hay otro ejemplo de retornar un status custom
app.get('/request5', (req, res) => {
  //res.status(300).send(req.query)
  res.send(req.query)
})

// Recibe parametro por GET retorna el registro donde si campo id conicide con el id a filtrar
app.get('/request6', (req, res) => {
  const result = data.filter(row => row.id === parseInt(req.query.id))
  res.send(result)
})

// Recibe parametro por POST retorna el registro donde si campo id conicide con el id a filtrar
app.post('/request7', (req, res) => {
  const result = data.filter(row => row.id === parseInt(req.body.id))
  res.send(result)
})

//retorna una pagina web HTML
app.get('/request8', (req, res) => {
  res.sendFile(path.join(__dirname+'/home.html'))
})

app.listen(port, () => {
  console.log(`Ejecutandose en la URL http://localhost:${port}`)
})
